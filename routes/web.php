<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/blog/category/{id?}', 'BlogController@categoryById')->name('category');
Route::get('/blog/item/{id?}', 'BlogController@itemById')->name('item');
Route::get('/cart/', 'CartController@index')->name('cart.index');
Route::post('/cart/add_to_cart/', 'CartController@addToCart')->name('cart.add_to_cart');
Route::post('/cart/remove_cart/', 'CartController@removeCartById')->name('cart.remove_cart');
Route::post('/cart/update_item_cart/', 'CartController@updateItemInCart')->name('cart.update_item_cart');
Route::post('/validationPost','FormController@validationPost')->name('validationPost');

Route::group( ['middleware' => 'auth' ], function()
{
    Route::get('/checkout/', 'CartController@checkout')->name('checkout');
    Route::post('/order/', 'CartController@order')->name('order');
    Route::post('/rating_star/', 'CartController@ratingStar')->name('rating_star');
    Route::post('/check_promo_code/', 'CartController@checkPromoCode')->name('checkPromoCode');

});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth','admin'] ], function () {
    Route::get('/', 'DashboardController@dashboard')->name('admin.index');
    Route::resource('/order', 'OrderController', ['as'=>'admin']);
    Route::resource('/item', 'ItemController', ['as'=>'admin']);
    Route::resource('/user', 'UserController', ['as'=>'admin']);
    Route::resource('/promo', 'PromoController', ['as'=>'admin']);
    Route::resource('/promotion', 'PromotionController', ['as'=>'admin']);
    Route::resource('/category', 'CategoryController', ['as'=>'admin']);
    Route::post('/dropzoneUpload', 'ItemController@dropzoneUploadImages')->name('admin.dropzoneUpload');
    Route::post('/dropzoneDelete', 'ItemController@dropzoneDeleteImages')->name('admin.dropzoneDelete');
    Route::post('/remove_item_img/', 'ItemController@removeItemImgById')->name('admin.remove_item_img');
    Route::post('/remove_user_img/', 'UserController@removeUserImgById')->name('admin.remove_user_img');
    Route::post('/remove_item_main_img/', 'ItemController@removeItemMainImgById')->name('admin.remove_item_main_img');

});
Route::get('/', 'BlogController@index', function () {
    return view('/blog.home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
