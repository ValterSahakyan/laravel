<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FormController extends Controller
{

    public function validationPost(Request $request){
        if (User::where('email', $request->email)->exists()) {
            return \Response::json(false);
        }else{
            return \Response::json(true);
        }
    }
}
