<?php

namespace App\Http\Controllers;

use App\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Promo;
use App\Vote;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;


class CartController extends Controller
{
    public function addToCart(Request $request){
        $item = json_decode(collect($request->content));
        $item->quantity = 1;
        $item->subtotal = $request->content['price'];
        session()->put('cart.items.' . $item->id, $item);
        $carts = Cart::getCart();
        return \Response::json( $carts , 200);
    }

    public function removeCartById(Request $request){
        session()->forget('cart.items.' . $request->id);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cart.index',[
            'carts' => Cart::getCart(),
        ]);
    }

    public function checkout(){
        return view('cart.checkout',[
            'user' => Auth::user(),
            'carts' => Cart::getCart(),
        ]);
    }

    protected function validator(array $data){
        return Validator::make($data, [
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'address' => 'required',
            'address_2' => 'required',
            'country' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'cc_name' => 'required',
            'cc_number' => 'required',
            'cc_cvv' => 'required',
            'cc_expiration' => 'required',
        ]);
    }

    public function order(Request $request){
        $this->validator($request->all())->validate();
        $cart_items = Cart::getCart();
        $user = Auth::user();
        return DB::transaction(function() use($user, $cart_items, $request) {
            try {
                DB::table('users')->where('id', $user->id)->update([
                    'address' => $request->address,
                    'address_2' => $request->address_2,
                    'country' => $request->country,
                    'state' => $request->state,
                    'zip' => $request->zip,
                ]);

                DB::table('payments')->insert([
                    'user_id' => $user->id,
                    'subtotal' => $request->subtotal,
                    'sale_percent' => $request->sale_percent,
                    'payment_method' => $request->payment_method,
                    'cc_name' => $request->cc_name,
                    'transaction_id' => 12,
                    'transaction_token' => 'sadsad',
                    'transaction_status' => 'success',
                ]);
                $payment_id = DB::getPdo()->lastInsertId();
                foreach ($cart_items as $item) {
                    DB::table('items')->where('id', $item->id)->decrement('available', $item->quantity);
                    DB::table('orders')->insert([
                        'user_id' => $user->id,
                        'item_id' => $item->id,
                        'quantity' => $item->quantity,
                        'price' => $item->price,
                        'subtotal' => $item->subtotal,
                        'sale' => $request->sale,
                        'payment_id' => $payment_id,
                    ]);
                }
                DB::commit();
                session()->forget('cart.items');
                return redirect('/')->with('msg', 'Thank you for your purchase!');
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->back()->with('msg', 'Something is wrong');
            }
        });
    }

    public function updateItemInCart(Request $request){
        $items = Cart::getCart();
        foreach ($items as &$item) {
            if ($item->id == $request->id) {
                $item->quantity = $request->quantity;
                $item->subtotal = $request->subtotal;
                session()->put('cart.items.' . $item->id, $item);
            }
        }
    }

    public function checkPromoCode(Request $request){
        $date = \Carbon\Carbon::now();
        if (session()->get('promo') != $request->promo){
            $result =  Promo::where('promo',$request->promo)->where('expiry_date' , '>=', $date)->first();
            if ($result) {
                $result['promo'] = $request->promo;
                dd($result);
                $result['promo_percent'] = $request->sale_percent;
                $result['new_price'] = intval($request->subtotal) - ((intval($request->subtotal) / 100) * intval($result->sale_percent));
                session()->put('promo', $result);
                return json_encode($result);
            }
        }
    }

    public function ratingStar(Request $request){
        $user = Auth::user();
        $vote = Vote::where('user_id','=',$user->id)->where('item_id','=',$request->item)->get();
        if (count($vote) == 0) {
            if ($request->item && $request->vote) {
                Vote::create([
                    'item_id' => $request->item,
                    'user_id' => $user->id,
                    'vote' => $request->vote,
                ]);
                return 'true';
            }else{
                return 'false';
            }
        }else{
            Vote::where('user_id','=',$user->id)->where('item_id', '=', $request->item)->update([
                'vote' => $request->vote,
            ]);
            return 'true';
        }

    }
}
