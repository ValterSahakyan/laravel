<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Item;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function dashboard(){
        return view('admin.dashboard',[
            'count_users'       => User::count(),
            'count_items'       => Item::count(),
            'count_categories'  => Category::count(),
        ]);

    }
}
