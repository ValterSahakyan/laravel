<?php

namespace App\Http\Controllers\Admin;

use App\Item;
use App\Category;
use App\ItemImage;
use App\ItemCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;


class ItemController extends Controller
{
    public $item_default_img = 'photo_default.png';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.item.index',[
            'items' => Item::where('deleted_at','0')->paginate(10),
            'default_img' => $this->item_default_img
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.item.create', [
            'item'    => [],
            'categories' => Category::where('deleted_at' , '=', '0')->get(),
            'delimiter'  => ''
        ]);
    }


    public function uploadImage(Request $request, Item $item){
        if ($request->hasFile('imagename')) {
            $images = $request->file('imagename');
            foreach ($images as $image){
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/items');
                $image->move($destinationPath, $name);
                ItemImage::create([
                    'item_id' => $item->id,
                    'image' => $name,
                ]);
            }
        }
    }

    public function dropzoneUploadImages(Request $request){
        $image = $request->file('file');
        $time = Carbon::now();
        $extension = $image->getClientOriginalName();
        $filename = $extension;
        $destinationPath = public_path('/uploads/items');
        $upload_success = $image->move($destinationPath, $filename);
        if ($upload_success) {
            ItemImage::create([
                'item_id' => $request->id,
                'image' => $filename,
            ]);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request, Item $item)
    {
        if ($request->hasFile('main_image')) {
            $main_image = $request->file('main_image');
            $name = $main_image->getClientOriginalName();
            $destinationPath = public_path('/uploads/items');
            $main_image->move($destinationPath, $name);
        }else{
            $name = $this->item_default_img;
        }
        $item = Item::create([
            'name' => $request->name,
            'desc' => $request->desc,
            'price' => $request->price,
            'created_by' => $request->created_by,
            'main_image' => $name,
            'available' => $request->available,
        ]);
        $this->uploadImage($request,$item);

        if($request->input('categories')) :
            $item->categories()->attach($request->input('categories'));
        endif;
        return redirect()->route('admin.item.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        return view('admin.item.show', [
            'item'   => $item,
            'categories' => Category::where('deleted_at' , '=', '0')->get(),
            'default_img' => $this->item_default_img,
            'delimiter'  => ''
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        return view('admin.item.edit', [
            'item'   => $item,
            'items' => Item::get(),
            'default_img' => $this->item_default_img,
            'categories' => Category::where('deleted_at' , '=', '0')->get(),
            'delimiter'  => ''
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Item $item)
    {
        if ($request->hasFile('main_image')) {
            $main_image = $request->file('main_image');
            $name = $main_image->getClientOriginalName();
            $destinationPath = public_path('/uploads/items');
            $main_image->move($destinationPath, $name);
        }elseif($item->main_image){
            $name = $item->main_image;
        }else{
            $name = $this->item_default_img;
        }
        Item::where('id',$item->id)->update([
            'name' => $request->name,
            'desc' => $request->desc,
            'price' => $request->price,
            'created_by' => $request->created_by,
            'main_image' => $name,
            'available' => $request->available,
        ]);

        $this->uploadImage($request,$item);
        $item->categories()->detach();
        if($request->input('categories')) :
            $item->categories()->attach($request->input('categories'));
        endif;

        return redirect()->route('admin.item.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Item $item){
        $item->categories()->detach();
        Item::where('id', $item->id)->update([
            'deleted_at' => '1',
        ]);
        return redirect()->route('admin.item.index');
    }

    public function removeItemImgById(Request $request){
        ItemImage::where('id', $request->id)->update([
            'deleted_at' => '1',
        ]);
    }

    public function dropzoneDeleteImages(Request $request){
        ItemImage::where('image', $request->image)->update([
            'deleted_at' => '1',
        ]);
    }

    public function removeItemMainImgById(Request $request){
        Item::where('id', $request->id)->update([
            'main_image' => $this->item_default_img,
        ]);
    }
}
