<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $image_name = 'default_u_image.png';

    public function index()
    {
        return view('admin.user.index', [
            'users' => User::where('deleted_at','0')->paginate(10)
        ]);
    }

    protected function validator(array $request)
    {
        return Validator::make($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.create', [
            'user' => [],
            'delimiter' => ''
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request->all())->validate();
        $image_name = $this->image_name;
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/users');
            $imagePath = $destinationPath . "/" . $image_name;
            $image->move($destinationPath, $image_name);
        }
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'last_name' => $request->last_name,
            'role' => $request->role,
            'balance' => $request->balance,
            'image' => $image_name,
            'password' => Hash::make($request->password),
        ]);
        return redirect()->route('admin.user.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.user.edit', [
            'user' => $user,
            'delimiter' => ''
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if ($request->image){
            $image_name = $request->image;
        }else{
            $image_name = $this->image_name;
        }
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/users');
            $image->move($destinationPath, $image_name);
        }

        User::where('email', $request->email)->update([
            'name' => $request->name,
            'email' => $request->email,
            'last_name' => $request->last_name,
            'role' => $request->role,
            'balance' => $request->balance,
            'image' => $image_name,
            'password' => Hash::make($request->password),
        ]);

        return redirect()->route('admin.user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user)
    {
        User::where('id', $user->id)->update([
            'deleted_at' => '1',
        ]);
        return redirect()->route('admin.user.index');
    }

    public function removeUserImgById(Request $request)
    {
        User::where('id', $request->id)->update([
            'image' => $this->image_name,]);
    }
}