<?php

namespace App\Http\Controllers;
use App\Category;
use App\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class BlogController extends Controller
{
    public $item_default_img = 'photo_default.png';

    public function index(){
        return view('blog.home',[
            'items' => Item::where('deleted_at','0')->where('available','>',0)->paginate(12),
            'default_img' => $this->item_default_img
        ]);
    }
    public function categoryById($id){
        $category = Category::where('id',$id)->first();
        return view('blog.category',[
            'category' => $category,
            'items'    => $category->items()->paginate(12),
            'default_img' => $this->item_default_img
        ]);
    }

    public function itemById($id){
        return view('blog.item',[
            'item' => Item::where('id',$id)->first(),
            'default_img' => $this->item_default_img
        ]);
    }
}
