<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'created_by', 'modified_by','deleted_at'];

    public function items()
    {
        return $this->morphedByMany('App\Item', 'item_category');
    }
}
