<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promo extends Model
{
    protected $fillable = ['id','title', 'promo','expiry_date','sale_percent', 'created_at', 'updated_at'];

    protected $dates = ['deleted_at'];

    public function setPromoAttribute($value) {

        $this->attributes['promo'] = Str::slug( mb_substr($this->title, 0, 5) . "-" . \Carbon\Carbon::now()->format('dHi'), '-');
    }
}
