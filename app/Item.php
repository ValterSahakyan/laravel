<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = ['name', 'desc','price','created_by', 'main_image', 'modified_by', 'available', 'deleted_at'];

    public function categories(){
        return $this->morphToMany('App\Category', 'item_category');
    }

    public function votes(){
        return $this->hasMany('App\Vote', 'item_id');
    }

    public function images() {
        return $this->hasMany('App\ItemImage', 'item_id')->where('deleted_at', '=', '0');
    }
    public function hasImages() {
        return $this->belongsToMany('App\ItemImage', 'item_images')->where('deleted_at', '=', '0');
    }
}
