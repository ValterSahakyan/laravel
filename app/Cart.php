<?php

namespace App;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\Model;


class Cart extends Model
{
    public static function getCart(){
        return Session::get('cart.items');
    }

}
