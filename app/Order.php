<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['user_id', 'item_id','quantity','price', 'subtotal', 'sale', 'payment_id'];

    public function getOrders(){
        return $this->belongsTo('App\Item', 'item_id');
    }

}
