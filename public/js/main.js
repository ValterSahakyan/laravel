$(document).ready(function () {
    $("body").on("click","#add_to_cart",function(){
        var id = $(this).data('id');
        var content = $(this).data('content');
        var url = "/cart/add_to_cart";
        $(this).prop('disabled', true);
        $.ajax(
            {
                url: url,
                type: "POST",
                datatype: "text",
                data:{id:id,content:content},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(cart){
                    $('.cart_count').html(parseInt($('.cart_count').text())+1)
                    $(".cart-items").append('' +
                        '<li class="cart_item_'+id+'">\n' +
                        '    <span class="item">\n' +
                        '        <span class="item-left">\n' +
                        '            <img src="/uploads/items/'+content.main_image+'" alt="" style="height: 50px; width: 50px;"/>\n' +
                        '            <span class="item-info">\n' +
                        '                <span>'+content.name+'</span>\n' +
                        '                <span>'+content.price+' $</span>\n' +
                        '            </span>\n' +
                        '        </span>\n' +
                        '        <span class="item-right">\n' +
                        '            <button class="btn btn-xs btn-danger pull-right" id="remove_item_in_cart" data-id="'+id+'">x</button>\n' +
                        '        </span>\n' +
                        '    </span>\n' +
                        '</li>')
                }

            })
    });

    $("body").on("click","#remove_item_in_cart",function(){
        var id = $(this).data('id');
        var url = "/cart/remove_cart";
        $.ajax(
            {
                url: url,
                type: "POST",
                datatype: "text",
                data:{id:id},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(){
                    $(".cart_item_"+id).remove();
                    $('.cart_count').html(parseInt($('.cart_count').text())-1)
                    $(function() {
                        $('button').filter(function(){
                            return $(this).data("id")   == id}).prop('disabled', false);
                    });
                }
            })
    });

    $("body").on('keyup mouseup', '#quantity', function () {
        var count = parseInt($(this).val());
        var id = $(this).data('id');
        var price = parseInt($(this).data('price'));
        $('.item_subtotal_' + id+' span').html(count * price);
        $('.item_subtotal_' + id+' input').val(count * price);
    });

    $("body").on("blur change","#quantity",function(){
        console.log('asdas')
        var id = $(this).data('id');
        var subtotal =  $('.item_subtotal_' + id+' input').val();
        var quantity =  parseInt($('.item_quantity_'+id+' input').val());
        $.ajax(
            {
                url: "/cart/update_item_cart",
                type: "POST",
                datatype: "text",
                data:{id:id,quantity:quantity,subtotal:subtotal},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            })

    });
    $('#update_cart').click(function() {
        location.reload();
    });

    $("body").on("click", "#redeem_promo", function (e) {
        var promo = $('#promo_code').val();
        var subtotal = $('#order_subtotal').val();
        $.ajax(
            {
                url: "/check_promo_code",
                type: "POST",
                datatype: "text",
                data: {promo: promo,subtotal:subtotal},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (respons) {
                    if (respons) {
                        var respons = jQuery.parseJSON(respons);
                        $('.promo_code_box_danger').css('display','none');
                        $('.promo_code_box_success').css('display','flex');
                        $(".promo_code_sale").html("- " + respons.sale_percent + " %");
                        var subtotal = $(".sale_subtotal").text();
                        $(".sale_subtotal").html(respons.new_price)
                        $('#order_subtotal').val(respons.new_price);
                        $('#sale_percent').val(respons.sale_percent);
                        $('.redeem_promo').fadeOut();
                    } else {
                        $('.promo_code_box_danger').css('display','block');
                        $('.promo_code_box_success').css('display','none');
                    }
                }
            })
    });


    $("body").on("click", ".rating_star", function () {
        var item = $(this).data('id');
        var vote = $(this).val();
        var lable = $(this);
        $.ajax(
            {
                url: "/rating_star",
                type: "POST",
                datatype: "text",
                data: {item: item,vote:vote},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (respons) {
                    if (respons == 'true'){
                        $("#rating"+item).load(location.href + " #rating"+item);
                    }else{
                        $('.rating'+item).prepend('<h6 class="alert-danger text-center">You already voted!</h6>')
                    }
                },
                error: function(xhr, status, error) {
                    if (xhr.responseJSON.message == 'Unauthenticated.'){
                        $(location).attr('href', '/login');
                    }

                }
            })

    });

    $('.zoom-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        image: {
            verticalFit: true,
            titleSrc: function(item) {
            }
        },
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function(element) {
                return element.find('img');
            }
        }

    });

});
