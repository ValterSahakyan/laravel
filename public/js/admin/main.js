$(document).ready(function() {

    $("#add_multi_image").click(function(){
        var html = $(".clone").html();
        $(".increment").after(html);
    });
    $("body").on("click","#remove_multi_image",function(){
        $(this).parents(".control-group").remove();
    });


    $('#datepicker').datepicker({
        format: 'yyyy-mm-dd hh:mm:ss',
        uiLibrary: 'bootstrap4'
    });

});