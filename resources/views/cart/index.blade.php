@extends('layouts.app')

@section('content')
    <form class="container">
        <h2>Cart</h2>
        <table class="table table-striped">
            <thead>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Subtotal</th>
            <th>Image</th>
            <th class="text-right">Action</th>
            </thead>
                <tbody>
                @php
                    $subtotal = 0;
                @endphp
                @isset($carts)
                    @forelse ($carts as $cart)
                        @php
                            $subtotal+= $cart->subtotal;
                        @endphp
                        <tr class="cart_item_{{$cart->id}}">
                            <td>{{$cart->name}}</td>
                            <td>{{ str_limit($cart->desc, $limit = 30, $end = ' ...') }}</td>
                            <td>$ {{$cart->price}}</td>
                            <td class="item_quantity_{{$cart->id}}">
                                <input type="number" class="form-control col-8" data-id="{{$cart->id}}" size="6" name="quantity" id="quantity" min="1" max="{{$cart->available}}"
                                       data-id="{{$cart->id}}" data-price="{{$cart->price}}" value="{{$cart->quantity or 1}}"  />
                            </td>
                            <td class="item_subtotal_{{$cart->id}}">
                                $ <span class="item_subtotal">{{$cart->subtotal or $cart->price}}</span>
                                <input type="hidden" name="item_subtotal" value="{{$cart->subtotal or $cart->price}}">
                            </td>
                            <td>
                                <img src="/uploads/items/{{$cart->main_image}}" style="width: 100px; height: 100px;"/>
                            </td>
                            <td>
                                <button class="btn btn-sm btn-danger pull-right" type="button" id="remove_item_in_cart" data-id="{{$cart->id}}">x</button>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="12" class="text-center"><h2>No data available</h2></td>
                        </tr>
                    @endforelse
                @endisset
                </tbody>
        </table>
        <hr/>
        <div class="col-6">
            <div class="pull-left">
                <input type="button" class="btn btn-outline-secondary" id="update_cart" value="Update shopping cart">
            </div>
        </div>
    <div class="col-6 pull-right">
        <h3>Cart totals</h3>
        <table class="table table-striped table-responsive-sm">
            <thead>
            <th>Subtotal</th>
            <th><b>Total</b></th>
            </thead>
            <tbody>
                <td class="sum_subtotal">$
                    @if(!empty($subtotal))
                       {{$subtotal}}
                    @endif
                </td>
                <td class="sum_total">
                    <b>$
                        @if(!empty($subtotal))
                            {{$subtotal}}
                        @endif
                    </b>
                </td>
            </tbody>
            <tfoot>
            <tr>
                <td><a href="{{ url('/') }}" type="button" id="continue_shopping_btn" class="btn btn btn-dark"><i class="fa fa-arrow-left"></i> Continue shopping</a></td>
                <td colspan="12" class="pull-right">
                    <a type="button" class="btn btn-dark" id="checkout_btn" href="{{route('checkout')}}">Proceed to checkout <i class="fa fa-arrow-right"></i></a>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    </div>

@endsection