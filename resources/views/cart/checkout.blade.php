@extends('layouts.app')

@section('content')
    <div class="container">
        @if (\Session::has('msg'))
            <div class="alert alert-danger">
                {!! \Session::get('msg') !!}
            </div>
        @endif
        <form class="needs-validation" novalidate action="{{ route ('order') }}" method="post">
            @csrf
            <div class="row">
                <div class="col-md-4 order-md-2 mb-4">
                    <h4 class="d-flex justify-content-between align-items-center mb-3">
                        <span class="text-muted">Your cart</span>
                        <span class="badge badge-secondary badge-pill">{{count($carts)}}</span>
                    </h4>
                    <ul class="list-group mb-3">
                        @php
                            $subtotal = 0;
                        @endphp
                        @isset($carts)
                            @forelse ($carts as $cart)
                                @php
                                    $subtotal += $cart->subtotal;
                                @endphp
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                    <div>
                                        <h6 class="my-0">{{$cart->name}}</h6>
                                        <small class="text-muted">{{ str_limit($cart->desc, $limit = 10, $end = ' ...') }}</small>
                                    </div>
                                    <span class="text-muted"> x {{$cart->quantity}}</span>
                                    <span class="text-muted">$ {{$cart->subtotal}}</span>
                                </li>
                            @empty
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                    <h2>No data</h2>
                                </li>
                            @endforelse
                        @endisset
                        <?php if (!empty(Session::get('promo'))){
                            $promo = Session::get('promo')->new_price;
                            $promo_code = Session::get('promo')->promo;
                            $promo_percent = Session::get('promo')->sale_percent;
//                            dd(Session::get('promo'));
                         }else{
                            $promo = $subtotal;
                        }
                        ?>
                        <li class="list-group-item justify-content-between bg-light promo_code_box_success" style="display: flex;">
                            <div class="text-success">
                                <h6 class="my-0">Promo code</h6>
                                <small>{{$promo_code or ""}}</small>
                            </div>
                            <span class="text-success promo_code_sale">- {{$promo_percent or ""}} %</span>

                        </li>

                        <li class="list-group-item justify-content-between bg-light promo_code_box_danger" style="display: none;">
                            <div class="text-danger">
                                <h6 class="my-0">Expired Promo Code</h6>
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between">
                            <span>Total (USD)</span>
                            <strong class="">$ <span class="sale_subtotal">{{$promo}}</span></strong>
                            <input type="hidden" name="subtotal" id="order_subtotal" value="{{$promo}}">
                        </li>
                    </ul>
                    @if (empty(Session::get('promo')))
                        <div class="card p-2 redeem_promo">
                            <div class="input-group">
                                <input type="text" class="form-control" name="promo_code" id="promo_code" placeholder="Promo code">
                                <input type="hidden" class="form-control" name="sale_percent" id="sale_percent" value="">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-secondary" id="redeem_promo">Redeem</button>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-md-8 order-md-1">
                    <h4 class="mb-3">Billing address</h4>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="firstName">First name</label>
                            <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="firstName" placeholder="Name"  name="name" value="{{$user->name or ""}}" required>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="lastName">Last name</label>
                            <input type="text" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" id="lastName" name="last_name" placeholder="Last Name" value="{{$user->last_name or ""}}" required>
                            @if ($errors->has('last_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="email">Email</label>
                        <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" placeholder="Email" name="email" value="{{$user->email or ""}}" required>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <div class="invalid-feedback">Please enter a valid email address for shipping updates.</div>
                    </div>
                    <div class="mb-3">
                        <label for="address">Address</label>
                        <input type="text" class="form-control {{ $errors->has('address') ? ' is-invalid' : '' }}" id="address" name="address" placeholder="1234 Main St" value="{{$user->address or ""}}" required>
                        @if ($errors->has('address'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label for="address2">Address 2 <span class="text-muted">(Optional)</span></label>
                        <input type="text" class="form-control" id="address2" name="address_2" placeholder="Apartment or suite" value="{{$user->address_2 or ""}}">
                    </div>
                    <div class="row">
                        <div class="col-md-5 mb-3">
                            <label for="country">Country</label>
                            <input type="text" class="form-control {{ $errors->has('country') ? ' is-invalid' : '' }}" id="country" name="country" placeholder="Country" value="{{$user->country or ""}}" required>
                            @if ($errors->has('country'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('country') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="state">State</label>
                            <input type="text" class="form-control {{ $errors->has('state') ? ' is-invalid' : '' }}" id="state" name="state" placeholder="State" value="{{$user->state or ""}}" required>
                            @if ($errors->has('state'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('state') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="zip">Zip</label>
                            <input type="text" class="form-control {{ $errors->has('zip') ? ' is-invalid' : '' }}" id="zip" name="zip" placeholder="Zip" value="{{$user->zip or ""}}" required>
                            @if ($errors->has('zip'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('zip') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <hr class="mb-4">
                    <h4 class="mb-3">Payment</h4>
                    <div class="d-block my-3">
                        <div class="custom-control custom-radio">
                            <input id="credit" name="payment_method" value="credit_card" type="radio" class="custom-control-input" checked required>
                            <label class="custom-control-label" for="credit">Credit card</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input id="debit" name="payment_method" value="debit_card" type="radio" class="custom-control-input" required>
                            <label class="custom-control-label" for="debit">Debit card</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input id="paypal" name="payment_method" value="paypal" type="radio" class="custom-control-input" required>
                            <label class="custom-control-label" for="paypal">Paypal</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="cc-name">Name on card</label>
                            <input type="text" class="form-control {{ $errors->has('cc_name') ? ' is-invalid' : '' }}" id="cc-name" name="cc_name" placeholder="" required>
                            <small class="text-muted">Full name as displayed on card</small>
                            <div class="invalid-feedback">Name on card is required</div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="cc-number">Credit card number</label>
                            <input type="text" class="form-control {{ $errors->has('cc_number') ? ' is-invalid' : '' }}" id="cc-number" name="cc_number" placeholder="" required>
                            <div class="invalid-feedback">Credit card number is required</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 mb-3">
                            <label for="cc-expiration">Expiration</label>
                            <input type="text" class="form-control {{ $errors->has('cc_expiration') ? ' is-invalid' : '' }}" id="cc-expiration" name="cc_expiration" placeholder="" required>
                            <div class="invalid-feedback">Expiration date required</div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="cc-expiration">CVV</label>
                            <input type="text" class="form-control {{ $errors->has('cc_cvv') ? ' is-invalid' : '' }}" id="cc-cvv" name="cc_cvv" placeholder="" required>
                            <div class="invalid-feedback">Security code required</div>
                        </div>
                    </div>
                    <hr class="mb-4">
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to checkout</button>
                </div>
        </form>
    </div>
@endsection