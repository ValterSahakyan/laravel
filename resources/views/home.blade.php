@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Shop</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
                <div class="album py-5 bg-light">
                    <div class="container">
                        <div class="row">
{{--                            @forelse ($items as $item)--}}
                                <div class="col-md-4">
                                    <div class="card mb-4 box-shadow">
                                        <img class="card-img-top"  style="height: 225px; width: 100%; display: block;"  data-holder-rendered="true">
                                        <div class="card-body">
{{--                                            <h3>{{$item->name}}</h3>--}}
                                            {{--<h4>Price {{$item->price}} $</h4>--}}
                                                 {{--<p class="card-text">{{$item->desc}}</p>--}}
                                            {{--@foreach ($item->categories as $category_item)--}}
                                                {{--<span class="badge badge-info">{{$category_item->name}}</span>--}}
                                            {{--@endforeach--}}
                                            <div class="d-flex justify-content-between align-items-center">
                                                {{--<small class="text-muted">{{$item->created_at}}</small>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {{--@empty--}}
                                {{--<h2>No data available</h2>--}}
                            {{--@endforelse--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
