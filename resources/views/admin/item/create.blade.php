@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        <hr />
        <form class="form-horizontal" enctype="multipart/form-data" action="{{route('admin.item.store')}}" method="post">
            {{ csrf_field() }}
            <label for="">Name</label>
            <input type="text" class="form-control" name="name" placeholder="Item name" value="" required>
            <hr />
            <label for="">Description</label>
            <input type="text" class="form-control" name="desc" placeholder="Description" value="" required>
            <hr />
            <label for="">Price</label>
            <input type="number" class="form-control" name="price" placeholder="Price" value="" required>
            <hr />
            <label for="">Available</label>
            <input type="number" class="form-control" name="available" placeholder="Available" value="" required>
            <hr />
            <label for="">Category</label>
            <select class="form-control" name="categories[]" multiple="" required>
                @foreach ($categories as $category)
                    <option value="{{$category->id or ""}}">
                        {!! $delimiter or "" !!}{{$category->name or ""}}
                    </option>
                @endforeach
            </select>
            <hr/>
            <label for="">Main Image</label>
            <div class="input-group control-group" >
                <input type="file" name="main_image" class="form-control">
            </div>
            <hr />
            <label for="">Image</label>
            <div class="input-group control-group increment" >
                <input type="file" name="imagename[]" class="form-control">
                <div class="input-group-btn">
                    <button class="btn btn-success" id="add_multi_image" type="button"><i class="fa fa-plus"></i>Add</button>
                </div>
            </div>
            <div class="clone d-none">
                <div class="control-group input-group" style="margin-top:10px">
                    <input type="file" name="imagename[]" class="form-control">
                    <div class="input-group-btn">
                        <button class="btn btn-danger" type="button"><i class="fa fa-remove"></i> Remove</button>
                    </div>
                </div>
            </div>
            <input type="hidden" name="created_by" value="{{Auth::user()->id}}">
            <hr />
            <input class="btn btn-primary" type="submit" value="Save">
        </form>
    </div>

@endsection