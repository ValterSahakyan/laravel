@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        <div class="col-lg-8">
            <h1 class="mt-4">{{$item->name}}</h1>
            <hr>
            <p>{{$item->created_at}}</p>
            <hr>
            <img class="img-fluid rounded" src="/uploads/items/{{$item->main_image or $default_img}}" alt="">
            <hr>
            <div class="input-group control-group img_box">

                @foreach ($item->images as $key => $image)
                    <div class="col-2">
                        <img  src="/uploads/items/{{$image->image}}" style="width: 100px; height: 100px;" />
                    </div>
                @endforeach
            </div>
            <hr>
            <h2 class="mt-4">Price {{$item->price}} $</h2>
            <hr>
            <p class="lead">{{$item->desc}}</p>
            <hr>
            @if (count($item->categories) >= 1)
                <blockquote class="blockquote">
                    <p class="mb-0">Categories</p>
                    <footer class="blockquote-footer">
                        {{$item->categories->pluck('name')->implode(', ')}}
                    </footer>
                </blockquote>
            @endif
        </div>
    </div>
@endsection
