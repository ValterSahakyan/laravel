@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        <hr />
        <form class="form-horizontal" enctype="multipart/form-data" action="{{route('admin.item.update',$item)}}" method="post">
            <input type="hidden" name="_method" value="put">
            {{ csrf_field() }}
            <label for="">Name</label>{{$singer->name or ""}}
            <input type="text" class="form-control" name="name" placeholder="Item name" value="{{$item->name or ""}}" required>
            <hr />
            <label for="">Description</label>
            <input type="text" class="form-control" name="desc" placeholder="Description" value="{{$item->desc or ""}}" required>
            <hr />
            <label for="">Price</label>
            <input type="number" class="form-control" name="price" placeholder="Price" value="{{$item->price or ""}}" required>
            <hr />
            <label for="">Available</label>
            <input type="number" class="form-control" name="available" placeholder="Available" value="{{$item->available or ""}}" required>
            <hr />
            <label for="">Category</label>
            <select class="form-control" name="categories[]" multiple="">
                @foreach ($categories as $category)
                    <option value="{{$category->id or ""}}"
                        @isset($item->id)
                            @foreach ($item->categories as $category_item)
                                @if ($category->id == $category_item->id)
                                    selected="selected"
                                @endif
                            @endforeach
                        @endisset
                    >
                        {!! $delimiter or "" !!}{{$category->name or ""}}
                    </option>
                @endforeach
            </select>
            <hr/>
            <label for="">Main Image</label>
            <div class="input-group control-group main_img_box">
                    <div class="col-2 upload_img main_img{{$item->id}}">
                        <span class="remove_main_img" data-id="{{$item->id}}"><i class="fa fa-remove"></i></span>
                        <img  src="/uploads/items/{{$item->main_image or $default_img}}" style="width: 100px; height: 100px;" />
                    </div>
            </div>
            <hr/>
            <div class="input-group control-group" >
                <input type="file" name="main_image" class="form-control">
            </div>
            <hr />
            <label for="">Images</label>
            <div class="input-group control-group img_box">
                @foreach ($item->images as $image)
                    <div class="col-2 upload_img img{{$image->id}}">
                        <span class="remove_item_img" data-id="{{$image->id}}"><i class="fa fa-remove"></i></span>
                        <img  src="/uploads/items/{{$image->image or ""}}" style="width: 100px; height: 100px;" />
                    </div>
                @endforeach
            </div>
            <hr/>
            <div class="dropzone" id="my_dropzone">

            </div>
            <input type="hidden" name="modified_by" value="{{Auth::user()->id}}">
            <hr />
            <div class="input-group control-group">
                <input class="btn btn-primary" type="submit" value="Update">
            </div>
        </form>
    </div>
    <script type="text/javascript">
        $("body").on("click",".remove_main_img",function(){
            var id = $(this).data('id');
            $.ajax(
                {
                    url: "{{route('admin.remove_item_main_img')}}",
                    type: "POST",
                    datatype: "text",
                    data:{id:id},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                })
            $('.main_img'+id).remove();
        });

        $("body").on("click",".remove_item_img",function(){
            var id = $(this).data('id');
            $.ajax(
                {
                    url: "{{route('admin.remove_item_img')}}",
                    type: "POST",
                    datatype: "text",
                    data:{id:id},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                })
            $('.img'+id).remove();
        });

        Dropzone.autoDiscover = false;
        $(document).ready(function () {
            var id = "{{$item->id}}";
            console.log(id);
            $("#my_dropzone").dropzone({
                paramName: 'file',
                url:'{{route('admin.dropzoneUpload')}}',
                params:{id:id},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                maxFilesize: 5, // MB
                maxFiles: 20,
                acceptedFiles: ".jpeg,.jpg,.png,.gif",
                init: function() {
                    this.on("success", function(file, response) {

                    });
                },
                addRemoveLinks: true,
                dictRemoveFile:'remove',
                removedfile: function(file) {
                    $.ajax({
                        type: 'POST',
                        url: "{{route('admin.dropzoneDelete')}}",
                        data: {image: file.name, _token: $('[name="_token"]').val()},
                        dataType: 'html'
                    });
                    var _ref;
                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                }
            });

        })
    </script>
@endsection