@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        <hr>
        <a href="{{route('admin.item.create')}}" class="btn btn-primary pull-right">
            <i class="fa fa-plus-square-o"></i> Create Item
        </a>
        <table class="table table-striped">
            <thead>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Available</th>
            <th>Image</th>
            <th>Categories</th>
            <th class="text-right">Action</th>
            </thead>
            <tbody>
            @forelse ($items as $item)
                <tr>
                    <td>{{$item->name}}</td>
                    <td>{{ str_limit($item->desc, $limit = 30, $end = ' ...') }}</td>
                    <td>{{$item->price}}</td>
                    <td>{{$item->available}}</td>
                    <td>
                        <img  src="/uploads/items/{{$item->main_image or $default_img}}" style="width: 100px; height: 100px;" />
                    </td>
                    <td>
                        @foreach ($item->categories->where('deleted_at' , '=', '0') as $category_item)
                            <span class="badge badge-info">{{$category_item->name}}</span>
                        @endforeach
                    </td>
                    <td class="text-right">
                        <form onsubmit="if(confirm('Delete ?')){return true}else{return false}" action="{{route('admin.item.destroy',$item)}}" method="post">
                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field()}}
                            <a class="btn btn-info" href="{{route('admin.item.show', $item)}}">View</i></a>
                            <a class="btn btn-default" href="{{route('admin.item.edit', $item)}}"><i class="fa fa-edit"></i></a>
                            <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="12" class="text-center"><h2>No data available</h2></td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <td colspan="12">
                    <ul class="pagination pull-right">
                        {{ $items->links() }}
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>

@endsection