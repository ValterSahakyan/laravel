@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        <table class="table table-striped">
            <thead>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Image</th>
            <th>Quantity</th>
            <th>Subtotal</th>
            </thead>
            <tbody>
            @forelse ($orders as $order)
                <tr>
                    <td>{{$order->getOrders->name}}</td>
                    <td>{{ str_limit($order->getOrders->desc, $limit = 30, $end = ' ...') }}</td>
                    <td>{{$order->getOrders->price}} $</td>
                    <td>
                        <img  src="/uploads/items/{{$order->getOrders->main_image or ""}}" style="width: 100px; height: 100px;" />
                    </td>
                    <td>{{$order->quantity}}</td>
                    <td>{{$order->subtotal}} $</td>
                </tr>
            @empty
                <tr>
                    <td colspan="12" class="text-center"><h2>No data available</h2></td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <td colspan="12">
                    <ul class="pagination pull-right">
                        {{ $orders->links() }}
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>

@endsection
