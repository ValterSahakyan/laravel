@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        <hr>
        <a href="{{route('admin.user.create')}}" class="btn btn-primary pull-right">
            <i class="fa fa-plus-square-o"></i> Create User
        </a>
        <table class="table table-striped">
            <thead>
            <th>Name</th>
            <th>last Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Balance</th>
            <th>Image</th>
            <th class="text-right">Action</th>
            </thead>
            <tbody>
            @forelse ($users as $user)
                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->last_name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->role}}</td>
                    <td>{{$user->balance}} $</td>
                    <td><img  src="/uploads/users/{{$user->image or ""}}" style="width: 100px; height: 100px;" /></td>
                    <td class="text-right">
                        <form onsubmit="if(confirm('Delete ?')){return true}else{return false}" action="{{route('admin.user.destroy',$user)}}" method="post">
                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field()}}
                            <a class="btn btn-default" href="{{route('admin.user.edit', $user)}}"><i class="fa fa-edit"></i></a>
                            <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="12" class="text-center"><h2>No data available</h2></td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <td colspan="12">
                    <ul class="pagination pull-right">
                        {{ $users->links() }}
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>

@endsection