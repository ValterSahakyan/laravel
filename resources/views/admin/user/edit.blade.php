@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        <hr />
        <form class="form-horizontal" enctype="multipart/form-data" action="{{route('admin.user.update',$user)}}" method="post">
            <input type="hidden" name="_method" value="put">
            {{ csrf_field() }}
            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                <div class="col-md-6">
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Name"  name="name" value="{{ $user->name }}" required autofocus>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="last_name" class="col-md-4 col-form-label text-md-right">Last Name</label>
                <div class="col-md-6">
                    <input type="text" id="last_name" class="form-control" name="last_name" placeholder="Last Name" value="{{ $user->last_name }}" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                <div class="col-md-6">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" name="email" value="{{ $user->email }}" required>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="role" class="col-md-4 col-form-label text-md-right">Role</label>
                <div class="col-md-6">
                    <select class="form-control" id="role" name="role">
                        <option value="user"  @if ($user->role == 'user') selected="selected" @endif>User</option>
                        <option value="editor" @if ($user->role == 'editor') selected="selected" @endif>Editor</option>
                        <option value="admin" @if ($user->role == 'admin') selected="selected" @endif>Admin</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="balance" class="col-md-4 col-form-label text-md-right">{{ __('Balance') }}</label>
                <div class="col-md-6">
                    <input id="balance" type="number" min="0" class="form-control" placeholder="Balance" name="balance" value="{{ $user->balance or ""}}" required>
                </div>
            </div>
            <hr/>
            <div class="form-group row">
                <label for="image" class="col-md-4 col-form-label text-md-right">Image</label>
                <div class="col-2 upload_img u_img{{$user->id}}">
                    <span class="remove_user_img_btn" data-id="{{$user->id}}"><i class="fa fa-remove"></i></span>
                    <img  src="/uploads/users/{{$user->image or ""}}" style="width: 100px; height: 100px;" />
                    <input type="hidden" name="image" value="{{$user->image or ""}}" class="form-control">
                </div>
                <div class="col-md-4" >
                    <input type="file" name="image" id="image" value="{{$user->image or ""}}" class="form-control">
                </div>
            </div>
            <hr/>
            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                <div class="col-md-6">
                    <input id="password" type="password" placeholder="Password"  class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                <div class="col-md-6">
                    <input id="password-confirm" type="password" placeholder="Confirm Password"  class="form-control" name="password_confirmation" required>
                </div>
            </div>
            <hr />
            <div class="form-group row">
                <div class="col-md-12 text-md-center">
                    <input class="btn btn-primary" type="submit" value="Update">
                </div>
            </div>
        </form>
    </div>
    <script type="text/javascript">
        $("body").on("click", ".remove_user_img_btn", function () {
            var id = $(this).data('id');
            $.ajax(
                {
                    url: "{{route('admin.remove_user_img')}}",
                    type: "POST",
                    datatype: "text",
                    data: {id: id},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                })
            $('.u_img' + id).remove();
        });
    </script>
@endsection