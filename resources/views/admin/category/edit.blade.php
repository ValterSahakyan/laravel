@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        <hr />
        <form class="form-horizontal" action="{{route('admin.category.update',$category)}}" method="post">
            <input type="hidden" name="_method" value="put">
            {{ csrf_field() }}
            <label for="">Name</label>
            <input type="text" class="form-control" name="name" placeholder="Category name" value="{{$category->name or ""}}" required>
            <hr />
            <input type="hidden" name="modified_by" value="{{Auth::user()->id}}">
            <input class="btn btn-primary" type="submit" value="Update">
        </form>
    </div>

@endsection