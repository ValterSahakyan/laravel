@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        <hr />
        <form class="form-horizontal" action="{{route('admin.category.store')}}" method="post">
            {{ csrf_field() }}
            <label for="">Name</label>

            <input  type="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Category Name" name="name" value="{{ old('name') }}" required>
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
            <input type="hidden" name="created_by" value="{{Auth::user()->id}}">
            <hr />
            <input class="btn btn-primary" type="submit" value="Save">
        </form>
    </div>

@endsection