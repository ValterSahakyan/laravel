@extends('admin.layouts.app_admin')

@section('content')

<div class="container">
    <hr />
    <form class="form-horizontal" action="{{route('admin.promotion.store')}}" method="post">
        {{ csrf_field() }}
        <div class="col-12">
            <label for="">Name</label>
            <input type="text" class="form-control" name="name" placeholder="Item name" value="" required>
        </div>
        <hr />
        <div class="form-inline">
            <div class="col-6">
                <input type="text" class="form-control" name="expiry_date" id="datepicker" placeholder="Expiry Date" required>
            </div>
            <div class="col-6">
                <div class="input-group">
                    <input type="number" class="form-control" name="sale_price" id="" placeholder="Sale Price" value="" required>
                </div>
            </div>
        </div>
        <hr />
        <div class="col-12">
            <input class="btn btn-primary" type="submit" value="Save">
        </div>
    </form>
</div>
@endsection