@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        <hr>
        <a href="{{route('admin.promotion.create')}}" class="btn btn-primary pull-right">
            <i class="fa fa-plus-square-o"></i> Create Item
        </a>
        <table class="table table-striped">
            <thead>
            <th>Name</th>
            <th>Item</th>
            <th>Image</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Sale Price</th>
            <th>Expiry Date</th>
            <th class="text-right">Action</th>
            </thead>
            <tbody>


            </tbody>
            <tfoot>
            <tr>
                <td colspan="12">
                    <ul class="pagination pull-right">
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>

@endsection