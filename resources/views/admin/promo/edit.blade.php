@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        <hr />
        <form class="form-horizontal" action="{{route('admin.promo.update',$promo)}}" method="post">
            <input type="hidden" name="_method" value="put">
            {{ csrf_field() }}
            <div class="col-12">
                <label for="">Title</label>
                <input  type="text" class="form-control" placeholder="Title" name="title" value="{{$promo->title or ""}}" required>
            </div>
            <hr />
            <div class="col-12">
                <label for="">Promo Code (Unique value)</label>
                <input  type="text" class="form-control" placeholder="Automatic generation" name="promo" value="{{$promo->promo or ""}}" readonly="">
            </div>
            <hr />
            <div class="form-inline">
                <div class="col-6">
                    <label class="sr-only" for="datepicker">Expiry Date</label>
                    <input type="text" class="form-control" name="expiry_date" id="datepicker" placeholder="Expiry Date" value="{{$promo->expiry_date or ""}}" required>
                </div>
                <div class="col-6">
                    <label class="sr-only" for="sale_percent">Sale Percent</label>
                    <div class="input-group">
                        <input type="number" class="form-control" id="sale_percent" name="sale_percent" placeholder="Sale Percent" value="{{$promo->sale_percent or ""}}" required>
                        <div class="input-group-prepend">
                            <div class="input-group-text">%</div>
                        </div>
                    </div>
                </div>
            </div>
            <hr />
            <div class="col-6">
                <input class="btn btn-primary" type="submit" value="Update">
            </div>
    </div>
    </div>
@endsection