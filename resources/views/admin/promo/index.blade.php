@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        <hr>
        <a href="{{route('admin.promo.create')}}" class="btn btn-primary pull-right">
            <i class="fa fa-plus-square-o"></i> Create Promo Code
        </a>
        <table class="table table-striped">
            <thead>
            <th>Title</th>
            <th>Promo Code</th>
            <th>expiry_date</th>
            <th>Sale Percent</th>
            <th>Created at</th>
            <th>Updated at</th>
            <th class="text-right">Action</th>
            </thead>
            <tbody>
            @forelse ($promo_cods as $promo_cod)
                <tr
                   @if($date > date('Ymd', strtotime($promo_cod->expiry_date)))
                       class="alert-danger"
                           @endif>
                    <td>{{$promo_cod->title}}</td>
                    <td>{{$promo_cod->promo}}</td>
                    <td>{{$promo_cod->expiry_date}}</td>
                    <td>{{$promo_cod->sale_percent}} %</td>
                    <td>{{$promo_cod->created_at}}</td>
                    <td>{{$promo_cod->updated_at}}</td>
                    <td class="text-right">
                        <form onsubmit="if(confirm('Delete ?')){return true}else{return false}" action="{{route('admin.promo.destroy',$promo_cod)}}" method="post">
                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field()}}
                            <a class="btn btn-default" href="{{route('admin.promo.edit', $promo_cod)}}"><i class="fa fa-edit"></i></a>
                            <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="12" class="text-center"><h2>No data available</h2></td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <td colspan="12">
                    <ul class="pagination pull-right">
                        {{ $promo_cods->links() }}
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>

@endsection