@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="jumbotron">
                    <p><span class="label label-primary">Categories {{$count_categories}}</span></p>
                    <a class="btn btn-primary" href="{{ route('admin.category.create') }}">Create Category</a>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="jumbotron">
                    <p><span class="label label-primary">Items {{$count_items}}</span></p>
                    <a class="btn btn-primary" href="{{ route('admin.item.create') }}">Create Item</a>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="jumbotron">
                    <p><span class="label label-primary">Users {{$count_users}}</span></p>
                    <a class="btn btn-primary" href="{{ route('admin.user.create') }}">Create User</a>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="jumbotron">
                    <p><span class="label label-primary">My Orders </span></p>
                    <a class="btn btn-primary" href="{{ route('admin.order.index') }}">View Orders</a>
                </div>
            </div>
        </div>
    </div>
@endsection