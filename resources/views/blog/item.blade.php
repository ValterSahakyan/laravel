@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="row card_row">
                <aside class="col-sm-5 border-right">
                    <article class="gallery-wrap">
                        <div class="img-big-wrap">
                            <div class="zoom-gallery">
                                <a href="/uploads/items/{{$item->main_image or $default_img}}" data-source="/uploads/items/{{$item->main_image or $default_img}}">
                                    <img src="/uploads/items/{{$item->main_image or $default_img}}">
                                </a>
                            </div>
                        </div>
                        <div class="img-small-wrap row">
                            <div class="zoom-gallery">
                                @foreach ($item->images as $key => $image)
                                        <div class="item-gallery">
                                            <a href="/uploads/items/{{$image->image}}" data-source="/uploads/items/{{$image->image}}">
                                                <img src="/uploads/items/{{$image->image}}">
                                            </a>
                                        </div>
                                @endforeach
                            </div>
                        </div>
                    </article>
                </aside>
                <aside class="col-sm-7">
                    <article class="card-body p-5">
                        <h3 class="title mb-3">{{$item->name}}</h3>
                        <p class="price-detail-wrap">
                        <span class="price h3">
                            <span class="currency">US $</span><span class="num"> {{$item->price}}</span>
                        </span>
                        </p>
                        <dl class="item-property">
                            <dt>Description</dt>
                            <dd>
                                <p>
                                    {{$item->desc}}
                                </p>
                            </dd>
                        </dl>
                        <dl class="param param-feature">
                            <dt>Created At</dt>
                            <dd>{{$item->created_at}}</dd>
                        </dl>
                        <?php
                        if(count($item->votes) > 0){
                            $count = count($item->votes);
                            $sum = 0;
                            foreach ($item->votes as $key => $vote) {
                                $vote = $vote->vote;
                                $sum += $vote;
                            }
                            $average = $sum / $count;
                        }else{
                            $average = 0;
                        }
                        ?>
                        <dl class="param param-feature">
                            <dt>Rating</dt>
                            <dd>{{round($average,2)}}</dd>
                        </dl>
                        @if (count($item->categories) >= 1)
                        <dl class="param param-feature">
                            <dt>Categories</dt>
                            <dd>{{$item->categories->pluck('name')->implode(', ')}}</dd>
                        </dl>
                        @endif
                        <hr>
                        <div class="btn-group  ml-2">
                            <button type="button" data-id="{{$item->id}}" data-content="{{$item}}" id="add_to_cart"
                                    class="btn btn-lg text-uppercase"
                                    @if(!empty(Session::get('cart.items')))
                                    @foreach (Session::get('cart.items') as $cart_item)
                                    @if ($cart_item->id == $item->id)
                                    disabled="disabled"
                                    @endif
                                    @endforeach
                                    @endif
                            ><i class="fa fa-shopping-cart"></i> Add to Cart
                            </button>
                        </div>
                    </article>
                </aside>
            </div>
        </div>
    </div>
@endsection
