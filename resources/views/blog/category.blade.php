@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{$category->name}}</div>
                    <div class="album py-5 bg-light">
                        <div class="container">
                            <div class="row">
                                @forelse ($items as $item)
                                    <div class="col-md-4">
                                        <div class="card mb-4 box-shadow">
                                            <a  href="{{route('item',$item->id)}}">
                                                <img class="card-img-top" src="/uploads/items/{{$item->main_image or $default_img}}" style="height: 225px; width: 100%; display: block;"  data-holder-rendered="true">
                                            </a>
                                            <div class="card-body">
                                                <h3 class="text-center">{{$item->name}}</h3>
                                                <h4 class="text-center">$ {{$item->price}}</h4>
                                                <div class="rating">
                                                    <?php
                                                    for ($i = 5; $i >= 1; $i--) {
                                                        echo '<input type="radio" id="star'.$i.'-'.$item->id.'" class="rating_star" data-id="'.$item->id.'" name="rating" value="'.$i.'" />
                                                            <label class = "full" for="star'.$i.'-'.$item->id.'"></label>';
                                                    }
                                                    ?>
                                                </div>
                                                <br>
                                                <div class="btn-group btn-group-justified">

                                                    <div class="">
                                                        <a  href="{{route('item',$item->id)}}" type="button" class="btn more_btn">More</a>
                                                    </div>
                                                    <div class="ml-1">
                                                        <button type="button" data-id="{{$item->id}}" data-content="{{$item}}" id="add_to_cart" class="btn"
                                                                @if(!empty(Session::get('cart.items')))
                                                                @foreach (Session::get('cart.items') as $cart_item)
                                                                @if ($cart_item->id == $item->id)
                                                                disabled="disabled"
                                                                @endif
                                                                @endforeach
                                                                @endif
                                                        ><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    <h2 class="text-center">No data available</h2>
                                @endforelse
                            </div>
                        </div>
                    </div>

                </div>
                <ul class="pagination pull-right">
                    {{ $items->links() }}
                </ul>
            </div>
        </div>
    </div>
@endsection
