    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            <span class="cart_count">{{count($carts)}}</span> - Items<span class="caret"></span>
        </a>

        <ul class="dropdown-menu dropdown-cart" role="menu">
            <div class="cart-items">
                @isset($carts)
                    @forelse ($carts as $cart)
                        <li class="cart_item_{{$cart->id}}">
                            <span class="item">
                                <span class="item-left">
                                    <img src="/uploads/items/{{$cart->main_image}}" alt="" style="height: 50px; width: 50px;"/>
                                    <span class="item-info">
                                        <span>{{$cart->name}}</span>
                                        <span>{{$cart->price}} $</span>
                                    </span>
                                </span>
                                <span class="item-right">
                                    <button class="btn btn-xs btn-danger pull-right" id="remove_item_in_cart" data-id="{{$cart->id}}">x</button>
                                </span>
                            </span>
                        </li>
                    @empty
                        <h2 class="text-center">No data</h2>
                    @endforelse
                @endisset
            </div>
            <li class="divider"></li>
            <hr/>
            <li class="text-md-center">
                <a href="{{ route('cart.index')}}" type="button" class="btn btn-lg view_cart_btn">View Cart</a>
            </li>
        </ul>
    </li>

