    @foreach ($categories as $category)
        <li class="nav-item">
            <a class="nav-link" role="button"  aria-haspopup="true" aria-expanded="false" href="/blog/category/{{$category->id}}">{{$category->name}}</a>
        </li>
    @endforeach
