<footer>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/libs/jquery-validate/1.17.0/jquery.validate.min.js') }}" defer></script>
    <script src="{{ asset('js/main.js') }}" defer></script>
    <script src="{{ asset('js/libs/jquery.magnific-popup.js') }}" defer></script>
</footer>